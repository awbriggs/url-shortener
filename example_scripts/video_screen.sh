#!/bin/bash

# Entirely designed to be used for a mac at the moment
source $HOME/.bash_script_profile

base_file_name="/tmp/$(head -c10 < /dev/random | base64 | tr -cd 'a-z0-9')"
mov_random_file_name="$base_file_name.mov"
mp4_random_file_name="$base_file_name.mp4"

/usr/sbin/screencapture -V10 ${mov_random_file_name}
ffmpeg -i ${mov_random_file_name} ${mp4_random_file_name}
echo "File created and converted to mp4 ${mp4_random_file_name}"

echo "Posting video to https://link.austinbriggs.dev/link/file"
http -f --print=b POST https://link.austinbriggs.dev/link/file file@${mp4_random_file_name} | jq '.newUrl' | sed "s/\"//g" | pbcopy
echo "Put in clipboard"

osascript -e "display notification \"Video link in clipboard\" with title \"Link copied\""