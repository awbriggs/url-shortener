#!/bin/bash

# Entirely designed to be used for a mac at the moment. RE: oascript/pbcopy
source $HOME/.bash_script_profile

random_file_name="/tmp/$(head -c10 < /dev/random | base64 | tr -cd 'a-z0-9').txt"

pbpaste > "$random_file_name"

http -f --print=b POST https://link.austinbriggs.dev/link/file file@${random_file_name} | jq '.newUrl' | sed "s/\"//g" | pbcopy

osascript -e "display notification \"Link in clipboard\" with title \"Link copied\""
