#!/bin/bash

# Entirely designed to be used for a mac at the moment
source $HOME/.bash_script_profile

# Note: Because of our use of httpie, it will automatically try to send the content type as part
# of it posting the form as long as we add the content type to the end.
random_file_name="/tmp/$(head -c10 < /dev/random | base64 | tr -cd 'a-z0-9').png"

/usr/sbin/screencapture -i ${random_file_name}
echo "File created in ${random_file_name}"

echo "Posting link to https://link.austinbriggs.dev/link/file"
http -f --print=b POST https://link.austinbriggs.dev/link/file file@${random_file_name} | jq '.newUrl' | sed "s/\"//g" | pbcopy
echo "Put in clipboard"

osascript -e "display notification \"Image link in clipboard\" with title \"Link copied\""
