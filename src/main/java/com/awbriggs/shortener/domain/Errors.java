package com.awbriggs.shortener.domain;

public enum Errors {
    UNKOWN_ERROR("Unknown Error has Occurred"),
    LINK_CREATION_ERROR("Error with creating link"),
    LINK_DESTRUCTION_ERROR("Error with destroying link");

    private final String errorDescription;

    Errors(final String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorDescription() {
        return this.errorDescription;
    }
}
