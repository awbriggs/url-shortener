package com.awbriggs.shortener.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DeleteUrlResponse {
    private final String error;
    private final Boolean deleted;

    public DeleteUrlResponse() {
        this.error = "";
        this.deleted = true;
    }

    public DeleteUrlResponse(final String error) {
        this.error = error;
        this.deleted = false;
    }

    public String getError() {
        return error;
    }

    public Boolean getDeleted() {
        return deleted;
    }

}
