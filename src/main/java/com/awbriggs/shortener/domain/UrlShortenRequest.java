package com.awbriggs.shortener.domain;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import java.io.Serializable;

public class UrlShortenRequest implements Serializable {

    private static final long serialversionUID = 1L;

    @NotNull
    @Pattern(regexp = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$")
    private String oldUrl;

    @Min(1)
    @Max(TimeConstants.MINUTES_IN_WEEK)
    private int minutesToExist;
    private String itemType;

    public UrlShortenRequest() {
        // Empty constructor
    }

    public UrlShortenRequest(String oldUrl) {
        this.oldUrl = oldUrl;
    }

    public String getOldUrl() {
        return oldUrl;
    }

    public void setOldUrl(String oldUrl) {
        this.oldUrl = oldUrl;
    }

    public int getMinutesToExist() {
        return minutesToExist;
    }

    public void setMinutesToExist(int minutesToExist) {
        this.minutesToExist = minutesToExist;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(final String itemType) {
        this.itemType = itemType;
    }

}
