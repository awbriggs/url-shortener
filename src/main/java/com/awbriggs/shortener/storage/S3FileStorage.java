package com.awbriggs.shortener.storage;

import java.io.IOException;
import java.util.Optional;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.awbriggs.shortener.keys.LinkKeyGenerator;

public class S3FileStorage implements FileStorage {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String bucketName;
    private final AmazonS3 s3Client;
    private final LinkKeyGenerator linkKeyGenerator;

    public S3FileStorage(AmazonS3 s3Client, String bucketName, LinkKeyGenerator linkKeyGenerator) {
        this.s3Client = s3Client;
        this.bucketName = bucketName;
        this.linkKeyGenerator = linkKeyGenerator;
    }

    @Override
    public Optional<String> storeFile(final MultipartFile multipartFile, final int existenceTime) {
        String key = linkKeyGenerator.get();
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(multipartFile.getContentType());
        metadata.setContentLength(multipartFile.getSize());

        try {
            s3Client.putObject(bucketName, key, multipartFile.getInputStream(), metadata);
        } catch (IOException ex) {
            logger.debug("Error storing file in s3", ex);
            return Optional.empty();
        }

        return Optional.of(s3Client.generatePresignedUrl(new GeneratePresignedUrlRequest(bucketName, key)
                .withMethod(HttpMethod.GET)
                .withExpiration(DateTime.now().plusMinutes(existenceTime).toDate())).toString());
    }
}
