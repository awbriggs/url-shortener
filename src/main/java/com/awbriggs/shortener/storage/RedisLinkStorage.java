package com.awbriggs.shortener.storage;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.springframework.data.redis.core.RedisTemplate;

import com.awbriggs.shortener.domain.Link;
import com.awbriggs.shortener.keys.LinkKeyGenerator;

public class RedisLinkStorage implements LinkStorage {

    private final RedisTemplate<String, Object> redisTemplate;
    private final LinkKeyGenerator linkKeyGenerator;

    public RedisLinkStorage(final RedisTemplate<String, Object> redisTemplate, final LinkKeyGenerator generator) {
        this.redisTemplate = redisTemplate;
        this.linkKeyGenerator = generator;
    }

    @Override
    public Link getLink(final String id) {
        return (redisTemplate.hasKey(id)) ? (Link) redisTemplate.opsForValue().get(id) : null;
    }

    @Override
    public String addLink(final Link item) {
        final String newKey = getUnusedKey();
        redisTemplate.opsForValue().set(newKey, item, item.getDuration(), TimeUnit.SECONDS);
        return newKey;
    }

    @Override
    public boolean deleteLink(final String id) {
        return redisTemplate.delete(id);
    }

    private String getUnusedKey() {
        return Stream.generate(linkKeyGenerator)
                .limit(50)
                .filter((possibleKey) -> !redisTemplate.hasKey(possibleKey))
                .findFirst().orElse(null);
    }

}
