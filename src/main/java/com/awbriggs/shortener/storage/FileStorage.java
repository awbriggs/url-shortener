package com.awbriggs.shortener.storage;

import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

public interface FileStorage {
    Optional<String> storeFile(MultipartFile multipartFile, int existenceTime);
}
