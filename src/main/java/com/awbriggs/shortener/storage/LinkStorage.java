package com.awbriggs.shortener.storage;

import com.awbriggs.shortener.domain.Link;

public interface LinkStorage {
    Link getLink(String id);
    String addLink(Link item);
    boolean deleteLink(String id);
}
