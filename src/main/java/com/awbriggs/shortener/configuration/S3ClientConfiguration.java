package com.awbriggs.shortener.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.awbriggs.shortener.keys.LinkKeyGenerator;
import com.awbriggs.shortener.storage.FileStorage;
import com.awbriggs.shortener.storage.S3FileStorage;

@ConditionalOnProperty("imagestorage.S3BucketName")
@Configuration
public class S3ClientConfiguration {

    @Value("${S3_KEY}")
    private String key;

    @Value("${S3_SECRET}")
    private String secret;

    @Value("${imagestorage.S3BucketName}")
    private String bucketName;

    @Value("${imagestorage.S3Endpoint}")
    private String bucketEndpoint;

    @Value("${imagestorage.S3Region")
    private String bucketRegion;

    @Bean
    public FileStorage getS3ImageStorage(LinkKeyGenerator linkKeyGenerator) {
        return new S3FileStorage(AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(bucketEndpoint, bucketRegion))
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(key, secret)))
                .build(), bucketName, linkKeyGenerator);
    }

}
