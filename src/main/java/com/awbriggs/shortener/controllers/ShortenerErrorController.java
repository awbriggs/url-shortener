package com.awbriggs.shortener.controllers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ShortenerErrorController implements ErrorController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/error")
    public String error(HttpServletRequest httpServletRequest) {
        logger.warn("Error Page hit: Status Code: {}\nError Message: {}\n", httpServletRequest.getAttribute("javax.servlet.error.status_code"),
                httpServletRequest.getAttribute("javax.servlet.error.message")
        );
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

}
