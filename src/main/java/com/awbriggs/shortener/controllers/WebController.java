package com.awbriggs.shortener.controllers;

import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.ResourceAccessException;

import com.awbriggs.shortener.domain.Link;
import com.awbriggs.shortener.domain.ShortenedUrlResponse;
import com.awbriggs.shortener.domain.UrlShortenRequest;
import com.awbriggs.shortener.handler.UrlHandler;

@Controller
public class WebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private UrlHandler urlHandler;

    public WebController(UrlHandler urlHandler) {
        this.urlHandler = urlHandler;
    }

    @RequestMapping("/")
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("request", new UrlShortenRequest());
        return "index";
    }

    @PostMapping(value = "/page/link", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String formPost(UrlShortenRequest urlShortenRequest, ModelMap modelMap) {
        ShortenedUrlResponse urlResponse = urlHandler.getNewUrl(urlShortenRequest.getOldUrl(), urlShortenRequest.getMinutesToExist());
        if (urlResponse.hasError()) {
            logger.warn("Failure with Url: {} Minutes to Exist: {}", urlShortenRequest.getOldUrl(), urlShortenRequest.getMinutesToExist());
            return "redirect:/error";
        }
        modelMap.addAttribute("new_url", urlResponse.getNewUrl());
        return "new_url";
    }

    @GetMapping("/link/{linkId}")
    public String getCurrentLink(@PathVariable("linkId") String linkId, ModelMap modelMap) {
        Link link = urlHandler.getExistingUrl(linkId);
        if (link != null && link.getContentType() == null) {
            return "redirect:" + link.getUrl();
        } else if (link != null && link.getContentType() != null) {
            String contentType = link.getContentType().split("/")[0];
            modelMap.addAttribute("new_url", link.getUrl());
            switch (contentType) {
                case "image":
                    return "image";
                case "video":
                    modelMap.addAttribute("video_type", link.getContentType());
                    return "video";
                case "text":
                    modelMap.addAttribute("text", textGetText(link.getUrl()));
                    return "text";
            }
        }
        return "redirect:/notfound";
    }

    @RequestMapping("/notfound")
    public String notFound() {
        return "notfound";
    }

    @ExceptionHandler(ResourceAccessException.class)
    public void handleNotFoundLinks(ResourceAccessException exception, HttpServletResponse response) throws IOException {
        logger.error("Failure on access", exception);
        response.sendRedirect("/notfound");
    }

    private String textGetText(final String url) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();
        try {
            return client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

}
