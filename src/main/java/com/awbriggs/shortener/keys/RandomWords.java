package com.awbriggs.shortener.keys;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

import com.awbriggs.shortener.utils.CommonPatterns;

public class RandomWords implements LinkKeyGenerator {
    private static final String WORDS_LOCATION = "/usr/share/dict/words";
    private static final int WORD_DEFAULT_CAPACITY = 256_000;

    private final List<String> listOfWords = new ArrayList<>(WORD_DEFAULT_CAPACITY);
    private final Random random = new Random();

    private final Optional<Integer> minLengthOfWord;
    private final Optional<Integer> maxLengthOfWord;

    public RandomWords(Integer minLengthOfWord, Integer maxLengthOfWord) {
        readInWords();
        this.minLengthOfWord = (minLengthOfWord == null) ? Optional.empty() : Optional.of(minLengthOfWord);
        this.maxLengthOfWord = (maxLengthOfWord == null) ? Optional.empty() : Optional.of(maxLengthOfWord);
    }

    @Override
    public String get() {
        while (true) {
            String randomWord = listOfWords.get(random.nextInt(listOfWords.size()));

            if (!CommonPatterns.IS_WORD.getPattern().matcher(randomWord).matches()) {
                continue;
            }

            if (minLengthOfWord.isPresent() && !(randomWord.length() > minLengthOfWord.get())) {
                continue;
            }

            if (maxLengthOfWord.isPresent() && !(randomWord.length() < maxLengthOfWord.get())) {
                continue;
            }

            return randomWord;
        }
    }

    private void readInWords() {
        try (Stream<String> stream = Files.lines(Paths.get(WORDS_LOCATION))) {
            stream.forEach(listOfWords::add);
        } catch (IOException ex) {
            throw new RuntimeException("Requirements not met: Unable to read in words", ex);
        }
    }

}
