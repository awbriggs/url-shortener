package com.awbriggs.shortener.keys;

import java.util.Random;
import java.util.stream.Collectors;

public class RandomLetters implements LinkKeyGenerator {
    private final Random random = new Random();

    @Override
    public String get() {
        return random.ints()
                .map(i -> i % 128)
                .filter(Character::isAlphabetic)
                .limit(12)
                .mapToObj(Character::toString)
                .collect(Collectors.joining());
    }

}
