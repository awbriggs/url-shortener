package com.awbriggs.shortener.keys;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class RandomWordsTest {

    private Pattern wordPattern = Pattern.compile("\\w+");

    @Test
    public void getRandomWords() {
        RandomWords testObj = new RandomWords(null, null);
        List<String> words = Stream.generate(testObj).limit(3).collect(Collectors.toList());
        assertTrue(isListOfWords(words));
    }

    @Test
    public void getRandomWordsMaxLength() {
        RandomWords testObj = new RandomWords(null, 5);
        List<String> words = Stream.generate(testObj).limit(3).collect(Collectors.toList());
        assertTrue(isListOfWords(words, 5));
    }

    @Test
    public void getRandomWordsMaxAndMinLength() {
        RandomWords testObj = new RandomWords(3, 5);
        List<String> words = Stream.generate(testObj).limit(3).collect(Collectors.toList());
        assertTrue(isListOfWords(words, 3, 5));
    }

    private boolean isListOfWords(List<String> possibleWords) {
        return possibleWords.stream().allMatch(possibleWord -> wordPattern.matcher(possibleWord).matches());
    }

    private boolean isListOfWords(List<String> possibleWords, int maxLength) {
        return possibleWords.stream().allMatch(possibleWord -> wordPattern.matcher(possibleWord).matches() && possibleWord.length() < maxLength);
    }


    private boolean isListOfWords(List<String> possibleWords, int minLength, int maxLength) {
        return possibleWords.stream().allMatch(possibleWord -> wordPattern.matcher(possibleWord).matches() && possibleWord.length() < maxLength && possibleWord.length() > minLength);
    }

}