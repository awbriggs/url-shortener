package com.awbriggs.shortener.keys;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.regex.Pattern;
import java.util.stream.IntStream;

import org.junit.Test;

public class RandomLettersTest {

    @Test
    public void testRandomLettersCreatedEachTime() {
        var randomLetters = new RandomLetters();
        assertNotEquals(randomLetters.get(), randomLetters.get());
    }

    @Test
    public void testValidRandomAlphanumeric() {
        var randomLetters = new RandomLetters();
        var alphaNumericRegex = "^[a-zA-Z0-9]+";
        var pattern = Pattern.compile(alphaNumericRegex);
        IntStream.range(0, 100).forEach(ignored -> assertTrue(pattern.matcher(randomLetters.get()).matches()));
    }

}