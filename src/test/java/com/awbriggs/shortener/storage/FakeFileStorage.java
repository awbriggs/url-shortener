package com.awbriggs.shortener.storage;

import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

public class FakeFileStorage implements FileStorage {

    @Override
    public Optional<String> storeFile(final MultipartFile multipartFile, final int existenceTime) {
        return Optional.of("https://www.google.com");
    }

}
