package com.awbriggs.shortener.domain;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.awbriggs.shortener.keys.RandomLetters;
import com.awbriggs.shortener.storage.InMemoryLinkStorage;

public class LinkTest {

    @Test
    public void reformatDefaultLinkStorageIfNeeded() {
        InMemoryLinkStorage linkMapper = new InMemoryLinkStorage(new RandomLetters());
        var link = Link.LinkBuilder.create().withUrl("google.com").withTimeOfExistence(5, TimeUnit.MINUTES).build();
        String id = linkMapper.addLink(link);
        assertEquals("https://google.com", linkMapper.getLink(id).getUrl());
    }

}