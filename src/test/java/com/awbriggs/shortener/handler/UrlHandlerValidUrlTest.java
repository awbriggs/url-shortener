package com.awbriggs.shortener.handler;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.awbriggs.shortener.configuration.ApplicationProperties;
import com.awbriggs.shortener.keys.RandomLetters;
import com.awbriggs.shortener.storage.FakeFileStorage;
import com.awbriggs.shortener.storage.InMemoryLinkStorage;

@RunWith(Parameterized.class)
public class UrlHandlerValidUrlTest {

    private ApplicationProperties applicationProperties;

    @Before
    public void setup() {
        applicationProperties = new ApplicationProperties();
        applicationProperties.setDomain("whatever");
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"w", false},
                {"googs.com", true},
                {"www.google.com", true},
                {"http://www.google.com", true},
                {"google.com", true},
                {"testing", false}
        });
    }

    @Parameterized.Parameter()
    public String url;

    @Parameterized.Parameter(1)
    public boolean expectedResult;

    @Test
    public void testValidLink() {
        UrlHandler urlHandler = new UrlHandler(new InMemoryLinkStorage(new RandomLetters()), applicationProperties, new FakeFileStorage());
        assertEquals(expectedResult, urlHandler.isValidUrlLink(url));
    }
}
