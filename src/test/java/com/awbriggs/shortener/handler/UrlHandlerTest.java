package com.awbriggs.shortener.handler;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;

import java.io.ByteArrayInputStream;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.awbriggs.shortener.configuration.ApplicationProperties;
import com.awbriggs.shortener.keys.RandomLetters;
import com.awbriggs.shortener.storage.FakeFileStorage;
import com.awbriggs.shortener.storage.InMemoryLinkStorage;

public class UrlHandlerTest {
    private ApplicationProperties applicationProperties;

    @Before
    public void setup() {
        applicationProperties = new ApplicationProperties();
        applicationProperties.setDomain("whatever");
    }

    @Test
    public void imageShouldReturnPrlResponse() throws Exception {
        MultipartFile multipartFile = new MockMultipartFile("file", new ByteArrayInputStream("whatever".getBytes()));
        UrlHandler urlHandler = new UrlHandler(new InMemoryLinkStorage(new RandomLetters()), applicationProperties, new FakeFileStorage());
        assertTrue(urlHandler.getNewUrl(multipartFile, 60).getNewUrl().contains("link"));
    }

    @Test
    public void imageShouldBeDeleted() {
        UrlHandler urlHandler = new UrlHandler(new InMemoryLinkStorage(new RandomLetters()), applicationProperties, new FakeFileStorage());
        var urlResponse = urlHandler.getNewUrl("http://www.google.com", 360);
        assertNotNull(urlResponse);
        urlHandler.deleteUrl(urlResponse.getNewUrl());
        assertNull(urlHandler.getExistingUrl(urlResponse.getNewUrl()));
    }

}
