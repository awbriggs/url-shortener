FROM openjdk:13-jdk-slim

COPY build/libs/url-shortener.jar app.jar

ENTRYPOINT ["java","-jar", "-Djava.net.preferIPv4Stack=true", "/app.jar"]